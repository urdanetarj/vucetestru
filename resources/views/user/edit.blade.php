@include('welcome')


    <div class="container">
        <div class="row">
            <div class="col-md-4">

            </div>
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">
                      <h1 class="text-md-left">User Information</h1>
                  </div>

                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-12 text-md-center">
                              <h4>Name: {{$users->name}}</h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12 text-md-center">
                              <h4>LastName: {{$users->lastname}}</h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12 text-md-center">
                              <h4>Email: {{$users->email}}</h4>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12 text-md-center">
                              <h4>Smartphone: {{$users->smartphone}}</h4>
                          </div>
                      </div>

                      <div class="row text-md-center">
                          <div class="col-md-12">
                              <a class="btn btn-warning" href="/users/{{$users->id}}/edit" style="width: 80px;">Edit</a>
                              <form method="POST" action="/users/{{$users->id}}">
                                  <input name="_method" type="hidden" value="DELETE">
                                  @csrf
                                  <button class="btn btn-danger" style="width: 80px;" type="submit">Delete</button>
                              </form>
                          </div>

                      </div>

                  </div>
              </div>
          </div>

        </div>
    </div>