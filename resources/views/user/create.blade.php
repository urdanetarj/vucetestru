@include('welcome')

    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-md-left">User Create</h5>
                    </div>
                    <div class="card-body">
                      <form action="/users"  method="POST">
                          @csrf
                          <div class="row">
                              <div class="col-md-2">
                                  <label>Name:</label>
                              </div>

                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" required placeholder="Place your name" name="name">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2">
                                  <label>LastName:</label>
                              </div>

                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" required placeholder="Place your LastName" name="lastname">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2">
                                  <label>Email:</label>
                              </div>

                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" placeholder="Place your Email" type="email" name="email">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2">
                                  <label>Smartphone:</label>
                              </div>

                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" placeholder="Place your Smartphone"  name="smartphone">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2">
                                  <label>ID Perfil:</label>
                              </div>

                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" placeholder="Enter the profile Id"  required  name="id_perfil">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2">
                                  <label>Password:</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="form-group">
                                      <input class="form-control text-center" type="password" name="password" required placeholder="Place your Password">
                                  </div>
                              </div>
                          </div>

                         <button type="submit" class="btn btn-primary">Register</button>
                      </form>



                    </div>
                </div>
            </div>
        </div>
    </div>
