@include('welcome')

        <div class="container">
            <div class="row">
              <div class="col-md-4"></div>

              <div class="col-md-8">
                  <div class="card">
                      <div class="card-header">
                          <h1 class="text-md-left">Edit User</h1>
                      </div>

                      <div class="card-body">
                          <form method="POST" action="/users/{{$users->id}}">
                              <input type="hidden" name="_method" value="PUT">
                                @csrf
                              <div class="row">
                                  <div class="col-md-2">
                                      <label>Name:</label>
                                  </div>
                                  <div class="col-md-8">
                                      <div class="form-group">
                                          <input class="text-center form-control" placeholder="Place your Name" name="name" type="text" value="{{$users->name}}">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-2">
                                      <label>LastName:</label>
                                  </div>
                                  <div class="col-md-8">
                                      <div class="form-group">
                                          <input class="text-center form-control" placeholder="Place your LastName" name="lastname" type="text" value="{{$users->lastname}}">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-2">
                                      <label>Email:</label>
                                  </div>
                                  <div class="col-md-8">
                                      <div class="form-group">
                                          <input class="text-center form-control" placeholder="Place your Email"  name="email" type="email" value="{{$users->email}}">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-2">
                                      <label>Smartphone:</label>
                                  </div>
                                  <div class="col-md-8">
                                      <div class="form-group">
                                          <input class="text-center form-control" placeholder="Place your Smartphone" name="smartphone" type="text" value="{{$users->smartphone}}">
                                      </div>
                                  </div>
                              </div>
                                <button type="submit" class="btn btn-primary">Update User</button>
                          </form>
                      </div>
                </div>
              </div>
            </div>
        </div>