@include('welcome')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">User List</h1>
            </div>
        </div>


        @if($message=Session::get('store'))
                <button type = "button" class = "close" data-dismiss = "alert" > × </button>
                 <strong> {{$message}} </strong>
            @endif

        @if($update=Session::get('update'))
            <button type = "button" class = "close" data-dismiss = "alert" > × </button>
            <strong> {{$update}} </strong>
            @endif



        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th class="">Id</th>
                            <th class="">Name</th>
                            <th class="">LastName</th>
                            <th class="">Email</th>
                            <th class="">Smartphone</th>
                            <th class="">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <div class="row">

                            <tr>
                                @foreach($users as $user)
                                        <th>{{$user->id}}</th>
                                        <td>{{$user->name}}</td>
                                    <td>{{$user->lastname}}</td>
                                        <td>{{$user->email}}</td>
                                    <td>{{$user->smartphone}}</td>
                                    <td>
                                        <a class="btn btn-primary" style="width: 60px;" href="/users/{{$user->id}}">Show</a>
                                    </td>
                            </tr>
                                @endforeach

                        </div>

                    </tbody>

                </table>


            </div>
        </div>


        {{$users->links()}}
    </div>