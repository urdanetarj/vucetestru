<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'perfils';


    protected $fillable =
        [
            'description'
        ];



    public function user(){
        return $this->belongsTo('App\User');
    }


}


