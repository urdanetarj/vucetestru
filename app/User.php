<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_perfil', 'name','lastname', 'email','smartphone','password'
    ];




    protected $table= 'users';

    /**
     *
     *
     *
     *
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function perfils(){

        return $this->hasMany('App\Perfil');
    }

}


